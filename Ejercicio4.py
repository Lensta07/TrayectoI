import math
cateto1 = float (input ('Ingrese el valor del primer cateto: '))
cateto2 = float (input ('Ingrese el valor del segundo cateto: '))
area = float (cateto1*cateto2)/2
hipotenusa = math.sqrt(cateto1*cateto1+cateto2*cateto2)
perimetro= (cateto1+cateto2+hipotenusa)

print ('El área del triángulo es: ', area)
print ('El perímetro del triángulo es: ', perimetro)
