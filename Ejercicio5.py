km_recorridos = float (input ('Inserte la cantidad de km recorridos: '))
precio_g = float (input ('Inserte el precio de la gasolina: '))
tiempo_h = float (input ('Inserte el tiempo que demoró en el viaje en horas: '))
tiempo_m = float (input ('Inserte el tiempo que demoró en el viaje en minutos: '))
consumo_de_gasolina_por_km_en_bs=precio_g/km_recorridos
consumo_de_gasolina_por_km_en_litros=consumo_de_gasolina_por_km_en_bs*precio_g
velocidad_media_en_km_h=km_recorridos/(tiempo_h+tiempo_m/60)
velocidad_media_en_m_s=km_recorridos*1000/(tiempo_h*60*60+tiempo_m*60)

print ('Valor de consumo de gasolina por km en litros: ' + repr (consumo_de_gasolina_por_km_en_litros))
print ('Valor de consumo de gasolina por km en bs: ' + repr (consumo_de_gasolina_por_km_en_bs))
print ('Valor de velocidad media en km h: ' + repr (velocidad_media_en_km_h))
print ('Valor de velocidad media en m s: ' + repr (velocidad_media_en_m_s))
print ()